// IntroDialogBox.cpp : implementation file
//

#include "stdafx.h"
#include "IntroDialogBox.h"
#include "afxdialogex.h"


// IntroDialogBox dialog

IMPLEMENT_DYNAMIC(IntroDialogBox, CDialogEx)

IntroDialogBox::IntroDialogBox(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_OLE_PROPPAGE_LARGE, pParent)
{

}

IntroDialogBox::~IntroDialogBox()
{
}

void IntroDialogBox::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(IntroDialogBox, CDialogEx)
END_MESSAGE_MAP()


// IntroDialogBox message handlers
