#pragma once


// IntroDialogBox dialog

class IntroDialogBox : public CDialogEx
{
	DECLARE_DYNAMIC(IntroDialogBox)

public:
	IntroDialogBox(CWnd* pParent = NULL);   // standard constructor
	virtual ~IntroDialogBox();

// Dialog Data
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_OLE_PROPPAGE_LARGE };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
